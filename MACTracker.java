package net.floodlightcontroller.mactracker;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.List;

import org.projectfloodlight.openflow.protocol.OFActionType;
import org.projectfloodlight.openflow.protocol.OFCapabilities;
import org.projectfloodlight.openflow.protocol.OFControllerRole;
import org.projectfloodlight.openflow.protocol.OFFactory;
import org.projectfloodlight.openflow.protocol.OFMessage;
import org.projectfloodlight.openflow.protocol.OFPortDesc;
import org.projectfloodlight.openflow.protocol.OFRequest;
import org.projectfloodlight.openflow.protocol.OFStatsReply;
import org.projectfloodlight.openflow.protocol.OFStatsRequest;
import org.projectfloodlight.openflow.protocol.OFStatsType;
import org.projectfloodlight.openflow.protocol.OFType;
import org.projectfloodlight.openflow.protocol.OFVersion;
import org.projectfloodlight.openflow.protocol.match.Match;
import org.projectfloodlight.openflow.types.DatapathId;
import org.projectfloodlight.openflow.types.EthType;
import org.projectfloodlight.openflow.types.IPAddress;
import org.projectfloodlight.openflow.types.IPv4Address;
import org.projectfloodlight.openflow.types.OFPort;
import org.projectfloodlight.openflow.types.TableId;
import org.projectfloodlight.openflow.types.TransportPort;
import org.projectfloodlight.openflow.types.U64;

import net.floodlightcontroller.core.FloodlightContext;
import net.floodlightcontroller.core.IOFMessageListener;
import net.floodlightcontroller.core.IOFMessageWriter;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.core.LogicalOFMessageCategory;
import net.floodlightcontroller.core.SwitchDescription;
import net.floodlightcontroller.core.internal.OFConnection;
import net.floodlightcontroller.core.internal.TableFeatures;
import net.floodlightcontroller.core.IListener.Command;
import net.floodlightcontroller.core.IOFConnection;
import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.core.module.FloodlightModuleException;
import net.floodlightcontroller.core.module.IFloodlightModule;
import net.floodlightcontroller.core.module.IFloodlightService;

import net.floodlightcontroller.core.IFloodlightProviderService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.TimeUnit;
import java.util.Set;
import java.util.Timer;

import net.floodlightcontroller.packet.Ethernet;
import net.floodlightcontroller.packet.IPacket;
import net.floodlightcontroller.packet.IPv4;
import net.floodlightcontroller.packet.TCP;
import net.floodlightcontroller.packet.UDP;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;
import com.google.common.hash.PrimitiveSink;
import com.google.common.util.concurrent.ListenableFuture;

import io.netty.buffer.ByteBuf;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.Math;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketAddress;
import java.net.URL;

import net.floodlightcontroller.statistics.StatisticsCollector;
import java.net.URLConnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class MACTracker implements IOFMessageListener, IFloodlightModule, IOFMessageWriter, OFStatsRequest, IOFSwitch {

	protected IFloodlightProviderService floodlightProvider;
	protected Set<Long> ipAddresses;
	protected Set<Long> macAddresses;
	protected static Logger logger;

	int[] counter = new int[27]; // quantity of packets type[i]
	int packet_sum; // Summary of packets received
	int packet_in = 0;
	int flow_mod = 0;
	int warning_counter = 0;
	int entropy_counter = 0;
	static float K = 1.35f; // multiplier K
	int HP = 0; // running out of idea to name variables

	long startTimer = 0;
	long endTimer = 0;
	long duration = 0;

	static Map<IPv4Address, Float> dest_IP_count = new HashMap<>(); // Store IP - packets counted
	static Map<IPv4Address, Float> source_IP_count = new HashMap<>();
	static Map<Long, Float> dest_port_count = new HashMap<>();
	static Map<Long, Float> source_port_count = new HashMap<>();

	static float dest_IP_entropy;
	static float source_IP_entropy;
	static float dest_port_entropy;
	static float source_port_entropy;

	Map<String, String> path = new HashMap<>(); // Store path (eth_src - eth_dst)
	Map<IPv4Address, String> ARP_table = new HashMap<>(); // Store ARP (IP - MAC)

	// float avg_entropy = 0.000f;
	float entropy_threshold = 1f;

	float avg_f_rate = 0.000f;
	float f_rate = 0.00f;
	float f_rate_threshold = 0;

	static float PacketCountThreshold = 0f;
	static float ByteCountThreshold = 0f;
	static float Duration_nSecThreshold = 0f;

	static ArrayList<Integer> PacketCountArray = new ArrayList<Integer>();
	static ArrayList<Integer> ByteCountArray = new ArrayList<Integer>();
	static ArrayList<Integer> Duration_nSecArray = new ArrayList<Integer>();

	static ArrayList<String> DatapathIdArray = new ArrayList<String>();

	ArrayList<Float> EntropyArray = new ArrayList<Float>();
	ArrayList<Float> RateArray = new ArrayList<Float>();
	ArrayList<IPv4Address> VictimArray = new ArrayList<>();
	static ArrayList<String> sentSwitchArray = new ArrayList<String>();
	static ArrayList<String> DpIdArray = new ArrayList<String>();

	IPv4Address sourceIP, destIP;
	Long sourceMAC, destMAC;

	boolean attackSuspected = false;
	boolean isUnderAttack = false;
	boolean hasGetDatapathIdArray = false;

	static String file_path = "";

	// String[] type = new String[27]
	// type[0] = "HELLO";
	// type[1]= "ECHO_REQUEST";
	// type[2]= "ECHO_REPLY";
	// type[3]= "EEXPERIMENTER";
	// type[4]= "FEATURE_REQUEST";
	// type[5]= "FEATURE_REPLY";
	// type[6]= "GET_CONFIG_REQUEST";
	// type[7]= "GET_CONFIG_REPLY";
	// type[8]= "SET_CONFIG";
	// type[9]= "PACKET_IN";
	// type[10]= "FLOW_REMOVED";
	// type[11]= "PORT_STATUS";
	// type[12]= "PACKET_OUT";
	// type[13]= "FLOW_MOD";
	// type[14]= "GROUP_MOD";
	// type[15]= "PORT_MOD";
	// type[16]= "TABLE_MOD";
	// type[17]= "BARRIER_REQUEST";
	// type[18]= "BARRIER_REPLY";
	// type[19]= "QUEUE_GET_CONFIG_REQUEST";
	// type[20]= "QUEUE_GET_CONFIG_REPLY";
	// type[21]= "ROLE_REQUEST";
	// type[22]= "ROLE_REPLY";
	// type[23]= "GET_ASYNC_REQUEST";
	// type[24]= "GET_ASYNC_REPLY";
	// type[25]= "SET_ASYNC";
	// type[26]= "METER_MOD";

	@Override
	public String getName() {
		return MACTracker.class.getSimpleName();
	}

	@Override
	public boolean isCallbackOrderingPrereq(OFType type, String name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCallbackOrderingPostreq(OFType type, String name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleServices() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<Class<? extends IFloodlightService>, IFloodlightService> getServiceImpls() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleDependencies() {
		Collection<Class<? extends IFloodlightService>> l = new ArrayList<Class<? extends IFloodlightService>>();
		l.add(IFloodlightProviderService.class);
		return null;
	}

	@Override
	public void init(FloodlightModuleContext context) throws FloodlightModuleException {
		floodlightProvider = context.getServiceImpl(IFloodlightProviderService.class);
		macAddresses = new ConcurrentSkipListSet<Long>();
		ipAddresses = new ConcurrentSkipListSet<Long>();
		logger = LoggerFactory.getLogger(MACTracker.class);
	}

	@Override
	public void startUp(FloodlightModuleContext context) {
		floodlightProvider.addOFMessageListener(OFType.PACKET_IN, this);
		floodlightProvider.addOFMessageListener(OFType.HELLO, this);
		floodlightProvider.addOFMessageListener(OFType.ECHO_REQUEST, this);
		floodlightProvider.addOFMessageListener(OFType.ECHO_REPLY, this);
		floodlightProvider.addOFMessageListener(OFType.EXPERIMENTER, this);
		floodlightProvider.addOFMessageListener(OFType.FEATURES_REQUEST, this);
		floodlightProvider.addOFMessageListener(OFType.FEATURES_REPLY, this);
		floodlightProvider.addOFMessageListener(OFType.GET_CONFIG_REQUEST, this);
		floodlightProvider.addOFMessageListener(OFType.GET_CONFIG_REPLY, this);
		floodlightProvider.addOFMessageListener(OFType.SET_CONFIG, this);
		floodlightProvider.addOFMessageListener(OFType.FLOW_REMOVED, this);
		floodlightProvider.addOFMessageListener(OFType.PORT_STATUS, this);
		floodlightProvider.addOFMessageListener(OFType.PACKET_OUT, this);
		floodlightProvider.addOFMessageListener(OFType.FLOW_MOD, this);
		floodlightProvider.addOFMessageListener(OFType.GROUP_MOD, this);
		floodlightProvider.addOFMessageListener(OFType.PORT_MOD, this);
		floodlightProvider.addOFMessageListener(OFType.TABLE_MOD, this);
		floodlightProvider.addOFMessageListener(OFType.BARRIER_REQUEST, this);
		floodlightProvider.addOFMessageListener(OFType.BARRIER_REPLY, this);
		floodlightProvider.addOFMessageListener(OFType.QUEUE_GET_CONFIG_REQUEST, this);
		floodlightProvider.addOFMessageListener(OFType.QUEUE_GET_CONFIG_REPLY, this);
		floodlightProvider.addOFMessageListener(OFType.ROLE_REQUEST, this);
		floodlightProvider.addOFMessageListener(OFType.ROLE_REPLY, this);
		floodlightProvider.addOFMessageListener(OFType.GET_ASYNC_REQUEST, this);
		floodlightProvider.addOFMessageListener(OFType.GET_ASYNC_REPLY, this);
		floodlightProvider.addOFMessageListener(OFType.SET_ASYNC, this);
		floodlightProvider.addOFMessageListener(OFType.METER_MOD, this);
		floodlightProvider.addOFMessageListener(OFType.STATS_REQUEST, this);
		floodlightProvider.addOFMessageListener(OFType.STATS_REPLY, this);

	}

	public static float Calc_avg_entropy(ArrayList EntropyArray) {
		// Calculate average entropy
		float t = 0.000f;

		for (int i = 0; i < EntropyArray.size(); i++) {
			t += i;
		}
		t = t / EntropyArray.size();
		return t;
	}

	public static float Calc_avg_f_rate(ArrayList RateArray) {
		// Calculate average f rate
		float t = 0.000f;

		for (int i = 0; i < RateArray.size(); i++) {
			t += i;
		}
		t = t / RateArray.size();
		return t;
	}

	public static boolean checkFrameMAC(String eth_src, String eth_dst) {
		final String PATTERN = "^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$";
		Pattern pattern = Pattern.compile(PATTERN);
		Matcher src_matcher = pattern.matcher(eth_src);
		Matcher dst_matcher = pattern.matcher(eth_dst);
		return (src_matcher.matches() && dst_matcher.matches());
	}

	public static boolean validateMAC(String dpid) {
		final String PATTERN = "^([0-9A-Fa-f]{2}[:-]{1}){7}([0-9A-Fa-f]{2})$";
		Pattern pattern = Pattern.compile(PATTERN);
		Matcher dpid_matcher = pattern.matcher(dpid);

		return dpid_matcher.find();
	}

	public static String getFlowTable(String dpid) throws IOException {
		// God ends here
		/** Get flow table associated to input dpid then return in String */

		String stringUrl = "http://127.0.0.1:8080/wm/core/switch/" + dpid.toString() + "/flow/json";

		String result = null;

		URL url = new URL(stringUrl);
		URLConnection uc;
		uc = url.openConnection();

		uc.setRequestProperty("X-Requested-With", "Curl");

		BufferedReader reader = new BufferedReader(new InputStreamReader(uc.getInputStream()));
		StringBuilder builder = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null) {
			builder.append(line);
			builder.append(System.getProperty("line.separator"));

		}

		result = builder.toString();

		// If result does not contains numbers, return null
		if (!result.matches(".*\\d+.*")) {
			return null;
		}

		System.out.println("Result = " + result);

		return result;

	}

	public static void printAllFlowTable() {
		// Try get get flow table from all the switches
		try {
			for (String dpid : DpIdArray) {

				String flowTable = getFlowTable(dpid);
				if (flowTable == null) {
					System.out.println("Failed to get flow table from switch with datapath ID = "
							+ dpid);
				} else {
					String output = extract_flow_table(flowTable);
					System.out.println("Output = \n" + output);
				}
				logger.info("Flow table = " + flowTable);

				TimeUnit.SECONDS.sleep(1);

			}
		} catch (Exception e) {
			System.out.println("Failed to print all flow tables");
		}
	}

	public static void updateFlowThreshold() throws IOException {

		for (String dpid : sentSwitchArray) {
			String FlowTable = getFlowTable(dpid);
			System.out.println("Function=updateFlowThreshold;FlowTable=" + FlowTable);
			String[] FlowTablePattern = FlowTable.split(",");

			for (String p : FlowTablePattern) {
				if (p.contains("packet_count")) {
					String t = p.replaceAll("[^0-9]", "");
					Integer i = Integer.parseInt(t);
					PacketCountArray.add(i);
				}

				if (p.contains("byte_count")) {
					String t = p.replaceAll("[^0-9]", "");
					Integer i = Integer.parseInt(t);
					ByteCountArray.add(i);
				}

				if (p.contains("duration_nsec")) {
					String t = p.replaceAll("[^0-9]", "");
					Integer i = Integer.parseInt(t);
					Duration_nSecArray.add(i);
				}
			}
		}
	}

	public static void calculateFlowThreshold() {

		// Calculate thresholds
		int total = 0;
		float AvgPacketCount = 0;
		float AvgByteCount = 0;
		float AvgDuration_nSec = 0;

		// Calculate packet_count threshold
		for (int i : PacketCountArray) {
			total += i;
		}
		AvgPacketCount = (float) total / PacketCountArray.size();
		PacketCountThreshold = AvgPacketCount * K;
		total = 0;

		// Calculate byte_count threshold
		for (int i : ByteCountArray) {
			total += i;
		}
		AvgByteCount = (float) total / ByteCountArray.size();
		ByteCountThreshold = AvgByteCount * K;
		total = 0;

		// Calculate duration threshold
		for (int i : Duration_nSecArray) {
			total += i;
		}
		AvgDuration_nSec = (float) total / Duration_nSecArray.size();
		Duration_nSecThreshold = AvgDuration_nSec * K;
		total = 0;

	}

	private static String extract_flow_table(String flowTable) {
		String[] flowArray = flowTable.split(",");
		String output = "";
		for (String flow : flowArray) {
			output += "Packet count = " + flowArray[3].replaceAll("[^0-9]", "") + "\n";
			output += "Byte count = " + flowArray[4].replaceAll("[^0-9]", "") + "\n";
			output += "Duration = " + flowArray[6].replaceAll("[^0-9]", "") + "\n";
			/*
			 * output += "Packet count = " + flow.substring(80, 81) + "\n"; output +=
			 * "Byte count = " + flow.substring(97, 99) + "\n"; output += "Duration nSec = "
			 * + flow.substring(137, 146) + "\n";
			 */
		}
		return output;
	}

	private static float calculate_entropy(Map map) {

		float entropy = 0f;

		for (Object key : map.keySet()) {
			float t = 0.000f;
			t = (float) map.get(key) / 50f;
			entropy += t * (float) Math.log(t);
			logger.info("--- > float math.log(t) = " + (float) Math.log(t));
		}

		entropy = -entropy;
		return entropy;
	}

	public static void CreateFile(String info, int i) {
		try {
			String text = info;
			file_path = "/tmp/traffic/attack_" + i + ".txt";
			logger.info("File saved : " + file_path);
			File file = new File(file_path);

			if (!file.exists()) {
				file.createNewFile();
			}
			
			FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(text);
			bw.newLine();
			bw.close();
			
		} catch (Exception e) {

		}
	}

	protected List<OFStatsReply> getSwitchStatistics(DatapathId switchId, OFStatsType statsType) {
		IOFSwitch sw = StatisticsCollector.switchService.getSwitch(switchId);
		ListenableFuture<?> future;
		List<OFStatsReply> values = null;
		Match match;
		int portStatsInterval = 10;

		if (sw != null) {
			OFStatsRequest<?> req = null;
			switch (statsType) {
			case FLOW:
				match = sw.getOFFactory().buildMatch().build();
				req = sw.getOFFactory().buildFlowStatsRequest().setMatch(match).setOutPort(OFPort.ANY)
						.setTableId(TableId.ALL).build();
				break;

			case TABLE:
				if (sw.getOFFactory().getVersion().compareTo(OFVersion.OF_10) > 0) {
					req = sw.getOFFactory().buildTableStatsRequest().build();
				}
				break;

			case TABLE_FEATURES:
				if (sw.getOFFactory().getVersion().compareTo(OFVersion.OF_10) > 0) {
					req = sw.getOFFactory().buildTableFeaturesStatsRequest().build();
				}
				break;

			default:
				logger.error("Stats Request Type {} not implemented yet", statsType.name());
				break;
			}

			try {
				if (req != null) {
					future = sw.writeStatsRequest(req);
					values = (List<OFStatsReply>) future.get(portStatsInterval * 1000 / 2,
							TimeUnit.MILLISECONDS);
				}

			} catch (Exception e) {
				logger.error("Failure retrieving statistics from switc	h {}. {}", sw, e);
			}
		}
		return values;
	}

	public void getDatapathIdArray() {
		String stringUrl = "http://127.0.0.1:8080/wm/core/controller/switches/json";
		URL url;
		String result = null;
		try {
			url = new URL(stringUrl);
			URLConnection uc;
			uc = url.openConnection();

			uc.setRequestProperty("X-Requested-With", "Curl");

			BufferedReader reader = new BufferedReader(new InputStreamReader(uc.getInputStream()));
			StringBuilder builder = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
				builder.append(System.getProperty("line.separator"));

			}
			result = builder.toString();
			System.out.println("Result = " + result);

			String[] entries = result.split("}");
			int num_entries = entries.length;

			for (int i = 0; i < num_entries; i++) {
				String pattern[] = entries[i].split(",");
				String tmp = pattern[3].substring(14, 37);
				DatapathIdArray.add(tmp.toString());
				logger.info("Added to DatappathIdArray : " + tmp);
			}

		} catch (IOException e) {
			logger.info("Failed to get DatapathIdArray");
		}

	}

	@Override
	public Command receive(IOFSwitch sw, OFMessage msg, FloodlightContext cntx) {

		boolean isTimerOn = false;
		boolean isFrameValid = false;
		boolean hasGetDatapathIdArray = false;
		boolean hasGetFlowTable = false;
		float dest_IP_entropy = 0.000f;
		int THRESHOLD = 50;
		int file_count = 1;

		// Add dpid to its array
		/*
		 * logger.info("DPID =  " + sw.getId()); logger.info("DPIP check = " +
		 * validateMAC(sw.getId().toString())); if (validateMAC(sw.getId().toString())
		 * == true && !DpIdArray.contains(sw.getId())) { DpIdArray.add(sw.getId()); }
		 * 
		 * DpIdArray.add(sw.getId());
		 */

		if (attackSuspected == true) {
			logger.info("Warning : your network is suspected to be under attack !!");
		}

		if (packet_in == 0 && isTimerOn == false) {
			startTimer = System.nanoTime();
			isTimerOn = true;
			logger.info("---------> start time : " + startTimer);
		}

		

		// Count packets categorized by type
		switch (msg.getType()) {
		case HELLO:
			counter[0] += 1;
			logger.info("Message type : {}	Quantity: {}", msg.getType().toString(), counter[0]);
			break;

		case ECHO_REQUEST:
			counter[1] += 1;
			logger.info("Message type : {}	Quantity: {}", msg.getType().toString(), counter[1]);
			break;

		case ECHO_REPLY:
			counter[2] += 1;
			logger.info("Message type : {}	Quantity: {}", msg.getType().toString(), counter[2]);
			break;

		case PACKET_IN:
			// Not sure whether the frame MAC is valid or not, so we'd better check them in
			// the first place

			
			
			Ethernet eth = IFloodlightProviderService.bcStore.get(cntx,
					IFloodlightProviderService.CONTEXT_PI_PAYLOAD);
			isFrameValid = checkFrameMAC(eth.getSourceMACAddress().toString(),
					eth.getDestinationMACAddress().toString());

			if (!isFrameValid) {
				// logger.info("Invalid frame's MAC address !! \n Controller will drop the frame
				// then continue.");
				return Command.CONTINUE;
			}

			packet_in += 1;
			logger.info("Message type : {}	Quantity: {}", msg.getType().toString(), packet_in);

			if (eth.getEtherType() == EthType.IPv4) {
				/* We got an IPv4 packet; get the payload from Ethernet */
				IPv4 ipv4 = (IPv4) eth.getPayload();
				destIP = ipv4.getDestinationAddress();
				sourceIP = ipv4.getSourceAddress();
				UDP UDP_Packet = (UDP) ipv4.getPayload();
				TransportPort UDP_Packet_dest_port = UDP_Packet.getDestinationPort();
				TransportPort UDP_Packet_source_port = UDP_Packet.getSourcePort();

				logger.info("UDP source port = " + UDP_Packet_source_port);

				String string_UDP_Packet_dest_port = UDP_Packet_dest_port.toString();
				long long_UDP_Packet_dest_port = Long.valueOf(string_UDP_Packet_dest_port);

				if (!dest_port_count.containsKey(UDP_Packet_source_port)) {
					dest_port_count.put(long_UDP_Packet_dest_port, 1f);
				} else {
					dest_port_count.put(long_UDP_Packet_dest_port,
							dest_port_count.get(long_UDP_Packet_dest_port) + 1);
					logger.info("dest_port_count =" + dest_port_count.get(long_UDP_Packet_dest_port) );

				}

				String string_UDP_Packet_source_port = UDP_Packet_source_port.toString();
				long long_UDP_Packet_source_port = Long.valueOf(string_UDP_Packet_source_port);

				if (!source_port_count.containsKey(UDP_Packet_source_port)) {
					source_port_count.put(long_UDP_Packet_source_port, 1f);
				} else {
					source_port_count.put(long_UDP_Packet_source_port,
							source_port_count.get(long_UDP_Packet_source_port) + 1);
					logger.info("source_port_count =" + source_port_count.get(long_UDP_Packet_source_port) );
				}

				logger.info("----------------------Print out dest_port_count ;)\n" + " }");
				for (Object key : dest_port_count.keySet()) {
					logger.info("Dest_port : {}", dest_port_count.keySet());
				}

				logger.info("----------------------Print out source_port_count ;)\n" + " }");
				for (Object key : source_port_count.keySet()) {
					logger.info("Source_port : {}", source_port_count.keySet());
				}

				// logger.info("Destination Address : {}", ipv4.getDestinationAddress());
/*
				sourceIP = ipv4.getSourceAddress();
				if (!ARP_table.containsKey(sourceIP)) {
					ARP_table.put(sourceIP, "this is a null value");
					// System.out.println("______>>>> " + ARP_table.get(sourceIP));
				}*/
				
				if (destIP == null || sourceIP == null) {
					// logger.info("Destination Address is null");
					return Command.CONTINUE;
				}

				if (!dest_IP_count.containsKey(destIP)) // List hosts in the topology
					dest_IP_count.put(destIP, 0f);
				else {
					// destIP found in dest_IP_count -> increase its counter by 01
					dest_IP_count.put(destIP, dest_IP_count.get(destIP) + 1);
				}
				
				if (!source_IP_count.containsKey(sourceIP)) // List hosts in the topology
					source_IP_count.put(sourceIP, 0f);
				else {
					// destIP found in dest_IP_count -> increase its counter by 01
					source_IP_count.put(sourceIP, source_IP_count.get(sourceIP) + 1);
				}

				logger.info("----------------------Print out dest_IP_count IPs;)\n" + " }");
				for (Object key : dest_IP_count.keySet()) {
					logger.info("DEST IP : {}", dest_IP_count.keySet());
				}

				logger.info("----------------------Print out source_IP_count IPs;)\n" + " }");
				for (Object key :source_IP_count.keySet()) {
					logger.info("SOURCE IP : {}", source_IP_count.keySet());
				}
			}

			
			
			// Calculate entropy and reset round counter
			if (packet_in == 50) {
				// getDatapathIdArray();
				if (hasGetDatapathIdArray == false) {
					logger.info("DatapathIdArray : ");
					DatapathIdArray.add("00:00:00:00:00:00:00:01");
					DatapathIdArray.add("00:00:00:00:00:00:00:02");
					DatapathIdArray.add("00:00:00:00:00:00:00:03");
					DatapathIdArray.add("00:00:00:00:00:00:00:04");
					hasGetDatapathIdArray = true;
					logger.info((DatapathIdArray.toString()));
				}
				
				// Stop timer
				endTimer = System.nanoTime();
				logger.info("---------> end time : " + endTimer);
				duration = (endTimer - startTimer);
				RateArray.add((float) (duration / 1000000));
				logger.info("-------------------> It took " + duration / 1000000 + " millis seconds");

				
				dest_IP_entropy = calculate_entropy(dest_IP_count);
				source_IP_entropy = calculate_entropy(source_IP_count);
				dest_port_entropy = calculate_entropy(dest_port_count);
				source_port_entropy = calculate_entropy(source_port_count);

				String content = source_IP_entropy + " " + dest_IP_entropy + " " + source_port_entropy + " "
						+ dest_port_entropy + " " + -1;

				CreateFile(content, file_count);
				file_count++;
				
				EntropyArray.add(dest_IP_entropy);
				float avg_entropy = Calc_avg_entropy(EntropyArray);
				entropy_threshold = avg_entropy * 0.7f;

				logger.info("-------------> Average Entropy = {}", avg_entropy);
				logger.info("-------------> Entropy threshold = {}", entropy_threshold);

				// Calculate f rate
				avg_f_rate = Calc_avg_f_rate(RateArray);
				f_rate_threshold = 20f;

				logger.info("-------------> Average f rate = {}", avg_f_rate);
				logger.info("-------------> F rate threshold = {}", f_rate_threshold);

				// Print out switches list
				// logger.info("SW list : --------------");
				/*
				 * for (DatapathId d : DpIdArray) {
				 * 
				 * }
				 */
				// System.out.println(DpIdArray.toString());

				if (dest_IP_entropy >= entropy_threshold) {
					if (f_rate <= f_rate_threshold) {
						warning_counter = 0;
						entropy_threshold = dest_IP_entropy;
						f_rate_threshold = avg_f_rate * 0.7f;

						logger.info("-------------> Warning counter has been reset");
						logger.info("-------------> Updated entropy threshold = {}", entropy_threshold);
						logger.info("-------------> Updated f_rate threshold = {}", f_rate_threshold);

					} else {
						warning_counter++;
					}

					if (warning_counter == 5) {
						attackSuspected = true;
					}

				} else {
					entropy_counter++;
					System.out.println("---> Entropy counter : " + entropy_counter);
					if (entropy_counter == 5)
						attackSuspected = true;
					entropy_counter = 0;
				}

				// Check DPID array
				System.out.println("Check DpId Array : ");
				System.out.println(DpIdArray.toString());

				printAllFlowTable();

				System.out.println("Threshold before updating : ");
				System.out.println("PacketCountThreshold = " + PacketCountThreshold);
				System.out.println("ByteCountThreshold = " + ByteCountThreshold);
				System.out.println("Duration_nSecThreshold = " + Duration_nSecThreshold);

				// Update then calculate a bunch of thresholds
				try {
					updateFlowThreshold();
				} catch (Exception e1) {
					System.out.println("Failed to update threshold");

				}

				try {
					calculateFlowThreshold();
				} catch (Exception e1) {
					System.out.println("Failed to calculate threshold");

				}

				System.out.println("Threshold after updating : ");
				System.out.println("PacketCountThreshold = " + PacketCountThreshold);
				System.out.println("ByteCountThreshold = " + ByteCountThreshold);
				System.out.println("Duration_nSecThreshold = " + Duration_nSecThreshold);

				// Reset counter
				packet_in = 0;
				isTimerOn = false;
				dest_IP_count.clear();
				source_IP_count.clear();
				dest_port_count.clear();
				source_port_count.clear();
			}
			
			// Check and add the host to the dest_IP_count list
			Long sourceMACHash = eth.getSourceMACAddress().getLong();

			if (!macAddresses.contains(sourceMACHash)) {
				macAddresses.add(sourceMACHash);
				// logger.info("MAC Address: {} seen on switch: {}",
				// eth.getSourceMACAddress().toString(),
				// sw.getId().toString());
			}

			
			
			/*
			 * logger.info("----------------------Print out ARP table;)\n" + " }"); for
			 * (Object key : ARP_table.keySet()) { IPv4Address ip = (IPv4Address) key; //
			 * String mac = ARP_table.get(ip); logger.info("IP : {}", ip + "\tMAC " +
			 * ARP_table.get(ip)); }
			 */
			break;

		case PACKET_OUT:
			/*
			 * counter[12] += 1; logger.info("Message type : {}	Quantity: {}",
			 * msg.getType().toString(), counter[12]); logger.info("Packet out dpid = " +
			 * sw.getId());
			 */
			break;

		/*
		 * case FLOW_MOD:
		 * 
		 * flow_mod += 1;
		 * logger.info("Attempt to print out the whole flow mod packet : ");
		 * System.out.println(msg.toString());
		 * 
		 * // Divide them apart for more readibility // pattern[2] = eth_src //
		 * pattern[3] = eth_dst String[] pattern = msg.toString().split(","); String
		 * src_MAC = pattern[2];
		 * 
		 * // MAC addresses looks like a mess, clean them up //
		 * "src_MAC : aa:bb:cc:dd:ee:ff" ---> "aa:bb:cc:dd:ee:ff" src_MAC =
		 * src_MAC.substring(src_MAC.lastIndexOf('=') + 1);
		 * System.out.println("----------> src s = " + src_MAC); String dst_MAC =
		 * pattern[3]; dst_MAC = dst_MAC.substring(dst_MAC.lastIndexOf('=') + 1);
		 * System.out.println("----------> src t = " + dst_MAC);
		 * 
		 * System.out.println("Pattern 9" + pattern[9]); System.out.println("Pattern 10"
		 * + pattern[10]); System.out.println("Pattern 11" + pattern[11]);
		 * System.out.println("Pattern 12" + pattern[12]);
		 * System.out.println("Pattern 8" + pattern[8]);
		 * 
		 * 
		 * isFrameValid = checkFrameMAC(src_MAC, dst_MAC); if (!isFrameValid) { //
		 * logger.info("Invalid frame's MAC address !! \n Controller will drop the frame
		 * // then continue."); return Command.CONTINUE; }
		 * 
		 * // Store MAC addresses to path hashmap path.put(src_MAC, dst_MAC); //
		 * logger.info("----------> print out path");
		 * 
		 * for (Entry<String, String> entry : path.entrySet()) { //
		 * System.out.println(entry.getKey() + "->" + entry.getValue()); }
		 * 
		 * for (int i = 0; i < pattern.length; i++) { //
		 * System.out.println("Pattern number" + i + " " + pattern[i]); }
		 */

		}

		logger.info("--------------------------------------------->");
		// logger.info("Next packet !!\" says the method");
		return Command.CONTINUE;
	}

	@Override
	public boolean write(OFMessage m) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Collection<OFMessage> write(Iterable<OFMessage> msgList) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <R extends OFMessage> ListenableFuture<R> writeRequest(OFRequest<R> request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <REPLY extends OFStatsReply> ListenableFuture<List<REPLY>> writeStatsRequest(
			OFStatsRequest<REPLY> request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void putTo(PrimitiveSink arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean equalsIgnoreXid(Object arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCodeIgnoreXid() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Builder createBuilder() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set getFlags() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OFStatsType getStatsType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OFType getType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OFVersion getVersion() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getXid() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeTo(ByteBuf arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public SwitchStatus getStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getBuffers() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void disconnect() {
		// TODO Auto-generated method stub

	}

	@Override
	public Set<OFActionType> getActions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<OFCapabilities> getCapabilities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<TableId> getTables() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SwitchDescription getSwitchDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SocketAddress getInetAddress() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<OFPortDesc> getEnabledPorts() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<OFPort> getEnabledPortNumbers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OFPortDesc getPort(OFPort portNumber) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OFPortDesc getPort(String portName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<OFPortDesc> getPorts() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<OFPortDesc> getSortedPorts() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean portEnabled(OFPort portNumber) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean portEnabled(String portName) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isConnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Date getConnectedSince() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DatapathId getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<Object, Object> getAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isActive() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public OFControllerRole getControllerRole() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasAttribute(String name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object getAttribute(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean attributeEquals(String name, Object other) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setAttribute(String name, Object value) {
		// TODO Auto-generated method stub

	}

	@Override
	public Object removeAttribute(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OFFactory getOFFactory() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImmutableList<IOFConnection> getConnections() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean write(OFMessage m, LogicalOFMessageCategory category) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<OFMessage> write(Iterable<OFMessage> msglist, LogicalOFMessageCategory category) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OFConnection getConnectionByCategory(LogicalOFMessageCategory category) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <REPLY extends OFStatsReply> ListenableFuture<List<REPLY>> writeStatsRequest(
			OFStatsRequest<REPLY> request, LogicalOFMessageCategory category) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <R extends OFMessage> ListenableFuture<R> writeRequest(OFRequest<R> request,
			LogicalOFMessageCategory category) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TableFeatures getTableFeatures(TableId table) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public short getNumTables() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public U64 getLatency() {
		// TODO Auto-generated method stub
		return null;
	}

}